# Star Trek Universe

# Ontologies

The following ontologies are used to define the Star Trek "universe":

* Cosmology - general (not specific to Star Trek) facts
  about the Universe
* StarTrek - facts about primary and parallel universes and stories
  (TV and movies)
* StarFleet - facts about Federation StarFleet entities: ships,
  personnel, weapons, etc

Each ontology defines a set of related RDF/OWL statements (logic, facts).

## English statements

Statements are "inputs" on which each ontology is based.
We don't have that capability yet, so I attach comments in English as
entities using the predicate ``rdfs:comment``.
To list all the comments, execute in SPARQL:

```
SELECT ?s ?l ?c WHERE {?s rdfs:label ?l . ?s rdfs:comment ?c }
```

Stardog displays the following results:

    s	l	c
    http://nextangles.com/cosmology	Ontology: cosmology	"The cosmology ontology names entities in the cosmos: galaxies, stars, etc"
    http://nextangles.com/cosmology#Galaxy	Galaxy	Many galaxies are known to exist
    http://nextangles.com/cosmology#MilkyWay	Milky Way Galaxy	"The ""Milky Way"" is the local / primary) galaxy "
    http://nextangles.com/cosmology#Universe	Universe	We know at least one universe exists. There may be more
    http://nextangles.com/startrek	Ontology: Star Trek	"Facts about the Star Trek cosmos, stories, characters, stories (TV and movies) etc"
    http://nextangles.com/startrek#DS9_Universe	Deep space9 Universe	"Events of ""Deep Space 9"" take place (generally) in the TOS / primary universe"
    http://nextangles.com/startrek#IntoDarkness_ParallelUniverse	JJAbrams universe	"Events of ""Into Darkness"" take place in a parallel universe"
    http://nextangles.com/startrek#TOS_PrimaryUniverse	Star Trek (original series) primary universe	"Events of ""Star Trek (TOS)"" take place in the future of ""our"" (known) universe"
    http://nextangles.com/startrek#Enterprise_Universe	Enterprise parallel universerdfs:comment	"Events of ""Enterprise"" stories generally take place in the primary universe"
